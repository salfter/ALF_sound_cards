ALF Sound Cards
===============

So far, this is a recreation of the ALF MC-16 sound card in KiCad.  I had
originally produced a schematic from the original documentation and redid
the PCB layout, but there is [an Applefritter thread](https://www.applefritter.com/content/alf-mc16-clone)
with gerbers reconstructed from the original layout, so I recently (2 Aug
23) imported those and mapped KiCad components onto it to get something much
closer to the original.

