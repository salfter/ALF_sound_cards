MC16 components
F4723B: Fairchild F4723 http://www.bitsavers.org/components/fairchild/_dataBooks/1975_Fairchild_MOS_CCD_Data_Book.pdf
  possibly equivalent to 74LS256? https://archive.org/details/bitsavers_motoroladaFASTandLSTTLData_35934218/page/n579/mode/2up
  pin	F4723	74LS256
   1	A0	A0
   2	A1	A1
   3	Da	Da
   4	O0a	O0a
   5	O1a	O1a
   6	O2a	O2a
   7	O3a	O3a
   8	Vss	GND
   9	O0b	O0b
  10	O1b	O1b
  11	O2b	O2b
  12	O3b	O3b
  13	Db	Db
  14	/E	/E
  15	CL	/CL
  16	Vdd	Vcc

DAC-76: PMI (Precision Monolithics) DAC-76 COMDAC (companding DAC)
        PMI bought out by Analog Devices in 1990
        possible substitute: AMD Am6070 (https://www.justnsnparts.com/nsn-parts/5962011851555/)